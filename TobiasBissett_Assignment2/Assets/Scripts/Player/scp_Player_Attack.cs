﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Attack : MonoBehaviour
{
    [Header("Overlap Cirlce Variable")]
    //centre of the blade
    public Transform overlapCirclePoint;
    //radius of the over lap cirle
    public float circleRadius;
    //will be used to attack the enemies
    public LayerMask enemyMask;
    [Header("References")]
    scp_Enemy_Skeleton_Movement skellyMove;

    scp_Enemy_SkeletonMaster skeleton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AttackLogic()
    {
        //returns all colliders in range of circle
        Collider2D[] enemyCollider = Physics2D.OverlapCircleAll(overlapCirclePoint.position, circleRadius, enemyMask);
        //will go through each enemy we collided with
        foreach(Collider2D enemy in enemyCollider)
        {
            

            if(enemy.transform.parent.GetComponent<scp_Enemy_SkeletonMaster>())
            {
                skeleton = enemy.transform.parent.GetComponent<scp_Enemy_SkeletonMaster>();

                skeleton.GetDamaged(1);

                scp_Enemy_Skeleton_Movement skellyMove= enemy.transform.parent.GetComponent<scp_Enemy_Skeleton_Movement>();

                StartCoroutine(skellyMove.StopTheEnemyForASetAmountOfTime(2));


            }
        }
    }

    private void OnDrawGizmosSelected ()
    {
        if (overlapCirclePoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(overlapCirclePoint.position, circleRadius);
            
    }
}
