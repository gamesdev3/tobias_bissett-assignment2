﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_AnimationController : MonoBehaviour
{

    //variable that references the animator
    private Animator anim;

    //reference to Player_Movement
    private scp_Player_Movement movement;

    // Start is called before the first frame update
    void Start()
    {
        //will find the animator
        anim = GetComponent<Animator>();
        //will find the script and store it in the variable
        movement = GetComponent<scp_Player_Movement>();
        
    }

    // Update is called once per frame
    void Update()
    {
        MovementParameter(); 
    }

    void MovementParameter()
    {
        //to find the absolute value of horizontal value
        int absoluteValue = Mathf.Abs((int)movement.horizontalValue);

        //will set the variable anim to the same as horizontalValue
        anim.SetInteger("Movement", absoluteValue);
    }

    public void Attack()
    {
        //will trigger the attack animation
        anim.SetTrigger("Attack");
    }

    public void Death()
    {
        anim.SetTrigger("Death");
    }

    public void Hurt()
    {
        anim.SetTrigger("Hurt");
    }

}
