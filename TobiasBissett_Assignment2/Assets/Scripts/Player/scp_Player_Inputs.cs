﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Inputs : MonoBehaviour
{

    //variable that references Control Scheme
    public scp_ControlScheme controls;
    //reference to the animation controller
    private scp_Player_AnimationController anim;
    //reference to player movement script
    scp_Player_Movement player_Movement;
    //reference to attack logic script
    [SerializeField]scp_Player_Attack attackLogic;

    // Start is called before the first frame update
    private void Awake()
    {
        //create new control scheme object
        controls = new scp_ControlScheme();

        //Movement Control
        controls.Player.HorizonalMovement.performed += _ => OnHorizontalMovement();
        //Dash Control
        controls.Player.Dash.performed += _ => OnDash();
        //Attack Control
        controls.Player.Attack.performed += _ => OnAttack();
        //Jump Control
        controls.Player.Jump.performed += _ => OnJump();

        //finds the animation controller script and stores it to the variable
        anim = GetComponent<scp_Player_AnimationController>();

        player_Movement = GetComponent<scp_Player_Movement>();

        attackLogic = GetComponentInChildren<scp_Player_Attack>();
    }

    private void OnEnable()
    {
        //Enables the controls
        controls.Enable();
    }

    private void OnDisable()
    {
        //Disables the controls
        controls.Disable();
    }

    void OnHorizontalMovement()
    {
        player_Movement.FlipThePlayer();
    }

    void OnDash()
    {
        Debug.Log("Dashed");
    }

    void OnAttack()
    {
        //Fire the attack animation
        anim.Attack();

        attackLogic.AttackLogic();
    }

    void OnJump()
    {
        Debug.Log("Jump Pressed");
    }

    
}
