﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Movement : MonoBehaviour
{
    

    //reference to the rigidbody
    private Rigidbody2D rb;

    //will store the direction of the button press
    public float horizontalValue;

    //variable to control how fast the player is going
    public float speed;

    //variable to deal with the flip
    private Vector2 flippedScale = new Vector2(-1f, 1);

    //reference to player inputs
    scp_Player_Inputs inputs;
    
   
    // Start is called before the first frame update
    void Start()
    {
        //Finds rigidbody and stores it in rb
        rb = GetComponent<Rigidbody2D>();

        inputs = GetComponent<scp_Player_Inputs>();
    }

    // Update is called once per frame
    void Update()
    {
        StoreHorizontalValue();
        MovePlayer();

    }

   

    void MovePlayer()
    {
        //will create a Vector2 that I will use to give force to the player
        Vector2 force = new Vector2(horizontalValue * speed * Time.deltaTime, 0);

        if(rb != null)
        {
            //will add force to the rigidbody
            rb.AddForce(force, ForceMode2D.Impulse);
        }
    }

    public void FlipThePlayer()
    {
        if (inputs.controls.Player.HorizonalMovement.ReadValue<float>() == -1)
        {
            transform.localScale = flippedScale;
        }
        else if(inputs.controls.Player.HorizonalMovement.ReadValue<float>() == 1)
        {
            transform.localScale = Vector2.one;
        }
    }

    void StoreHorizontalValue()
    {
        //Stores the current float direction
        horizontalValue = inputs.controls.Player.HorizonalMovement.ReadValue<float>();
    }
}
