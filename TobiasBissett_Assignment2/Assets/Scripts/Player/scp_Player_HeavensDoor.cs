﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_HeavensDoor : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "DoorToEnd")
        {
            FindObjectOfType<scp_Manager_Scene>().LoadWinScreen();
        }
    }
}
