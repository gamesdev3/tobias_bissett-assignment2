﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_LifeManager : MonoBehaviour
{
    //hp variable
    public int hp;
    //ref to anim manager
    scp_Player_AnimationController animController;
    //variable to store
    public bool isPlayerAlive = true;


    private void Start()
    {
        animController = GetComponent<scp_Player_AnimationController>();
    }
    public void removeHP(int numberOfHPToRemove)
    {
        hp -= numberOfHPToRemove;

        if(hp <= 0)
        {
            Death();
            
        }
        else
        {
            animController.Hurt();
        }
    }

    public void Death()
    {
        animController.Death();
        isPlayerAlive = false;
        //
        GetComponent<Collider2D>().enabled = false;
        Destroy(GetComponent<Rigidbody2D>());
        GetComponent<scp_Player_Inputs>().enabled = false;

        StartCoroutine(DeathAnimationAndThenLoadDeathScene()); 
    }

    IEnumerator DeathAnimationAndThenLoadDeathScene()
    {
        yield return new WaitForSeconds(2f);
        FindObjectOfType<scp_Manager_Scene>().LoadDeadScene();
    }
}
