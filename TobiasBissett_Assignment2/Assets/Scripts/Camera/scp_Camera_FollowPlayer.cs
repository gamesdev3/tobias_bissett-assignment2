﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Camera_FollowPlayer : MonoBehaviour
{
    //Variable to store target of the camera
    public Transform target;

    public Vector3 cameraoffset;
    //variable that will be used by lerp
    public float smoothSpeed = 0.25f;


    private void FixedUpdate()
    {
        CameraFollow();
    }

    void CameraFollow()
    {
        //Store desriedPosition
        Vector3 desiredPosition = target.position + cameraoffset;
        //will lerp the position
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);

        //will follow the player
        transform.position = smoothedPosition;

    }
}
