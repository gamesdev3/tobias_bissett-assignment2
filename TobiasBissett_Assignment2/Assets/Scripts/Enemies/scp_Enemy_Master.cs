﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Master : MonoBehaviour
{
    //enemy lives
    public int lives;

    //will be sprite that we use for the enemy
    public Sprite enemySprite;

    //name of the enemy
    public string enemyName;

    //ref to game manager
    scp_Manager_Game gm;

    protected virtual void Start()
    {
        gm = FindObjectOfType<scp_Manager_Game>();
    }
    public virtual void Death()
    {
        if(lives <= 0)
        {
            gm.EnemyKilledPlusOne();
        }
    }

    public virtual void GetDamaged(int numberofLivesLost)
    {
        //removes set number of lives
        lives -= numberofLivesLost;
    }

    protected void SelfDestruct(float timeBeforeGettingDestroyed)
    {
        Destroy(this.gameObject, timeBeforeGettingDestroyed);
    }
}
