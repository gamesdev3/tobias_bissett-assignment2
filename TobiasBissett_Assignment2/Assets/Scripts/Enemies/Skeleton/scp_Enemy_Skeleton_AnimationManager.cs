﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Skeleton_AnimationManager : scp_Enemy_AnimationMaster
{
    public Color damaged1;
    public Color damaged2;

    SpriteRenderer spriteR;

    protected override void Start()
    {
        base.Start();
        spriteR = GetComponent<SpriteRenderer>();
    }
    public void attackAnimation()
    {
        anim.SetTrigger("Attack");  
    }
    public void DeathAnimation()
    {
        anim.SetTrigger("Death");
    }

    public void DamagedAnimation()
    {
        anim.SetTrigger("Damaged");
    }

    public void SetMovementParameter(int movement)
    {
        anim.SetInteger("Movement", movement);
    }

    public void damage(int damagelevel)
    {
        switch (damagelevel)
        {
            case 2:
                spriteR.material.SetColor("Color", damaged1);
                break;
            case 1:
                spriteR.material.SetColor("Color", damaged2);
                break;
        }
    }
}
