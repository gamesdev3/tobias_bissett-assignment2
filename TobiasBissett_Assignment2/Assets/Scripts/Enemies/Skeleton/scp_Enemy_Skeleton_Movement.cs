﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Skeleton_Movement : MonoBehaviour
{

    //reference to rigidbody
    private Rigidbody2D skeletonrb;
    //value to control skeleton speed
    public float speed;
    //direction of skeleton
    public int direction = 1;

    //variable to store original speed
    public float originalSpeed;
    //reference to skeleton animation manager
    scp_Enemy_Skeleton_AnimationManager skellyanim;

    private bool canmove = true;

    // Start is called before the first frame update
    void Start()
    {
        skeletonrb = GetComponent<Rigidbody2D>();

        skellyanim = GetComponent<scp_Enemy_Skeleton_AnimationManager>();

        originalSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
       ;

        if (canmove)
        {
            MoveSkeleton();
        }
    }

    void MoveSkeleton()
    {
        //will create a Vector2 that I will use to give force to the player
        Vector2 force = new Vector2(direction * speed * Time.deltaTime, 0);
        //will add force to the rigidbody
        skeletonrb.AddForce(force, ForceMode2D.Impulse);

        skellyanim.SetMovementParameter((int)speed);
    }

    

    public IEnumerator fliptheskelly()
    {
        yield return new WaitForSeconds(2f);
        // reverse the direction
        direction *= -1;
        //gives speed to the skeleton
        speed = originalSpeed;
        //flip the enemy
        transform.localScale = new Vector3(transform.localScale.x * -1, 1, 1);
        
    }

    public IEnumerator StopTheEnemyForASetAmountOfTime(float time)
    {
        canmove = false;
        speed = 0;
        skellyanim.SetMovementParameter((int)speed);
        yield return new WaitForSeconds(time);
        canmove = true;
        speed = originalSpeed;
        skellyanim.SetMovementParameter((int)speed);
    }

    public void DeathStop()
    {
        canmove = false;

    }

    public void FlipInstant()
    {
        direction *= -1;
        //flip the enemy
        transform.localScale = new Vector3(transform.localScale.x * -1, 1, 1);
        canmove = true;
        speed = 0;
    }
}