﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Skeleton_AwarenessBack : MonoBehaviour
{
    //ref to movement script
    public scp_Enemy_Skeleton_Movement skellyMove;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            //flipme!
            skellyMove.FlipInstant();
        }
    }
}
