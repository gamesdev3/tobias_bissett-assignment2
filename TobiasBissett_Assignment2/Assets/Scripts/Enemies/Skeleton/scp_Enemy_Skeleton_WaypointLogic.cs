﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Skeleton_WaypointLogic : MonoBehaviour
{
    //ref to skel move scp

    public scp_Enemy_Skeleton_Movement skellyMove;
    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Waypoint" && gameObject.tag == "BodyTag")
        {
            skellyMove.speed = 0;

            StartCoroutine(skellyMove.fliptheskelly());
        }
    }
}
