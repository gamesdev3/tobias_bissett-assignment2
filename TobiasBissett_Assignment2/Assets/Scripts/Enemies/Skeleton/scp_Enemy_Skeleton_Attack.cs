﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Skeleton_Attack : MonoBehaviour
{
    [Header("References")]
    //ref to animation script 
    public scp_Enemy_Skeleton_AnimationManager skellyAnim;
    [Header("Overlay Circle Variables")]
    //centre of the blade
    public Transform overlapCirclePoint;
    //radius of the over lap cirle
    public float circleRadius;
    //will be used to attack the enemies
    public LayerMask enemyMask;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            skellyAnim.attackAnimation();
            //Attacks the player
            AttackLogic();
        }
    }

    public void AttackLogic()
    {
        //returns all colliders in range of circle
        Collider2D playerCollider = Physics2D.OverlapCircle(overlapCirclePoint.position, circleRadius, enemyMask);
        if (playerCollider.GetComponent<scp_Player_LifeManager>())
        {
            playerCollider.GetComponent<scp_Player_LifeManager>().removeHP(1);
        }

    }

    private void OnDrawGizmosSelected()
    {
        if (overlapCirclePoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(overlapCirclePoint.position, circleRadius);

    }
}
