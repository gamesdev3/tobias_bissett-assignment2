﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_SkeletonMaster : scp_Enemy_Master
{
    //reference to skeleton animations
    scp_Enemy_Skeleton_AnimationManager skellyanimations;
    //reference to the skeely movement script
    scp_Enemy_Skeleton_Movement skellyMove;


    protected override void Start()
    {
        base.Start();
        //will get aniamtion script
        skellyanimations = GetComponent<scp_Enemy_Skeleton_AnimationManager>();

        skellyMove = GetComponent<scp_Enemy_Skeleton_Movement>();
    }
    public override void Death()
    {
        base.Death();
        skellyanimations.DeathAnimation();
    }
    public override void GetDamaged(int numberofLivesLost)
    {
        base.GetDamaged(numberofLivesLost);

        if (lives > 0)
        {
            skellyanimations.DamagedAnimation();
            skellyanimations.damage(lives);
        }
        else
        {
            Death();
            skellyMove.DeathStop();
        }
    }

}
