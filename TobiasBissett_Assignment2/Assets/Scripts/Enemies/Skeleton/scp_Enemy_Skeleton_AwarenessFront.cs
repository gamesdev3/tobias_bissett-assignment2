﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Skeleton_AwarenessFront : MonoBehaviour
{
    //ref to movement
    public scp_Enemy_Skeleton_Movement skellyMove;
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Debug.Log("I can see you!");

            //attack

            //if player leaves range speed up
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StartCoroutine(SpeedUpForASetAmountOfTime(2));
        }
    }

    IEnumerator SpeedUpForASetAmountOfTime(float time)
    {
        skellyMove.speed = 40;
        yield return new WaitForSeconds(time);
        skellyMove.speed = skellyMove.originalSpeed;
    }
}
