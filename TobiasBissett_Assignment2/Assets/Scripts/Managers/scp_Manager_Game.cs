﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Manager_Game : MonoBehaviour
{
    //varaible to track enemies killed
    public int enemiesKilled = 0;
    // Start is called before the first frame update
    public static scp_Manager_Game gminstance;

    private void Awake()
    {
        if (gminstance != null)
        {
            Destroy(this.gameObject);
        }
        else if (gminstance == null)
        {
            gminstance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }
    public void EnemyKilledPlusOne()
    {
        enemiesKilled++;
    }
}
