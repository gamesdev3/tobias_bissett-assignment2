﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scp_Manager_UI : MonoBehaviour
{
    [Header("Players Variables")]
    public Slider playerHpSlider;

    //ref to player life script
    scp_Player_LifeManager playerLife;

    [Header("Score Varaibles")]
    public Text enemiesKilledText;

    private scp_Manager_Game gm;

    // Start is called before the first frame update
    void Start()
    {
        playerLife = FindObjectOfType<scp_Player_LifeManager>();

        playerHpSlider.maxValue = playerLife.hp;

        playerHpSlider.value = playerLife.hp;

        gm = FindObjectOfType<scp_Manager_Game>();
        enemiesKilledText.text = gm.enemiesKilled.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        //
        playerHpSlider.value = playerLife.hp;
        enemiesKilledText.text = gm.enemiesKilled.ToString();
    }
}
